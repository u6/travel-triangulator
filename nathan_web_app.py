import json # Yes, we're using this instead of a DB. I know.
import os # This is to show filenames and shit like that.
import itty3
import nathan_travel_tri_function

TravelApp = itty3.App() # Object creation function for itty3

@TravelApp.get('/')
def index(request):
    with open("nathan_index.html") as index_file:
        template = index_file.read()

    with open("temp_results.json") as data_file:
        data = json.load(data_file)

    content = ''

    # Make the list that we'll be pushing in later and replacing
    # {{ content }} with.
    for offset, item in enumerate(data.get("temperatures", [])):
        content += '<li>{}</li>'.format(item)

    # Find and replace "{{ content }}" in the HTML with our new update
    template = template.replace('{{ content }}', content)

    return TravelApp.render(request, template)

@TravelApp.post('/update_temp/')
def update_temperatures(request):
    # Load JSON file from disk
    with open("temp_results.json") as data_file:
        data = json.load(data_file)

    temperature_location = request.POST.get("update_temp", "---").strip()

 
    print (temperature_location)

    print(nathan_travel_tri_function.temperature_criteria(temperature_location, 80))

    resulting_temp = nathan_travel_tri_function.temperature_criteria(temperature_location, 80)

    data["temperatures"].append(resulting_temp)

    with open("temp_results.json", "w") as data_file:
        json.dump(data, data_file, indent=4)


    return TravelApp.redirect(request, "/")

if __name__ == "__main__":
    TravelApp.run()
