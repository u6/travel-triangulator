#!/usr/bin/env python3

import requests
import secrets

locations_dict = {'OBX':['Outer Banks', '35.5669444444', '-75.4686111111'], 'MIM':['Miami', '76.7', '-20.0']}

def temperature_criteria(location_chosen, temperature_chosen):
    
    ## Assign the parameters to the variables where they'll get used
    destination = location_chosen
    temp_input = temperature_chosen

    print ("Enter your preferred destination from the list of available locations")
    print (locations_dict)
#    destination = input(">>>")
    
    if destination not in locations_dict: print('wtf check your locations')
    
    destination_data = locations_dict[destination]
    
    destination_name = destination_data[0]
    destination_latitude = destination_data[1]
    destination_longitude = destination_data[2]
    
    print("Alright, let's figure out how to get you and your friends to " + destination_name)
    
    # Find Temperatures
    
    months_list = ['Jan', 'Feb', 'May', 'Jun', 'Jul', 'Oct']
    
    if int(temp_input) > 100 or int(temp_input) < 0: print("ERROR! Come on, now. Pick a real temperature.")
    
    my_totally_secure_api_key = secrets.my_totally_secure_api_key
    
    def temperature_getter(latit, longit):
        # Normally we'd send an API request here: https://openweathermap.org/api/one-call-api#current
        # But for now we'll fake it with some dummy data
        result_of_api_request = requests.get(url="https://api.openweathermap.org/data/2.5/onecall?lat=" + latit + "&lon=" + longit + "&exclude=weekly,hourly,daily&appid=" + my_totally_secure_api_key)
        result_dict = result_of_api_request.json()
        temperature_result = result_dict['current']['temp']
        return temperature_result - 273.15 # subtracting to convert from Kelvin to Celsius
    
    return "Temperature is " +str(temperature_getter(destination_latitude, destination_longitude))
